use std::iter;

use csp_solve::problem::{all_diff, BinaryConstraint, VarId, Variable};
use csp_solve::solve::solve;

const SIZE: usize = 9;
const N_BLOCKS: usize = 3;
const BLOCK_SIZE: usize = SIZE / N_BLOCKS;

fn draw_solution(grid: Vec<Vec<Variable>>, assignment: Vec<u32>) {
    for column in grid {
        for item in column {
            print!("{} ", assignment[item.id.0])
        }
        print!("\n\n");
    }
}

fn create_constraints(grid: &Vec<Vec<Variable>>) -> Vec<BinaryConstraint> {
    let column_constraints = (0..SIZE).flat_map(|x| all_diff(&grid[x]));
    let row_constraints =
        (0..SIZE).flat_map(|y| all_diff(&grid.iter().map(|row| row[y]).collect::<Vec<Variable>>()));

    let mut blocks: Vec<Vec<VarId>> = vec![vec![]; N_BLOCKS * N_BLOCKS];

    for x in 0..SIZE {
        for y in 0..SIZE {
            let block_x = x / N_BLOCKS;
            let block_y = y / N_BLOCKS;

            let block_idx = block_x + N_BLOCKS * block_y;
            blocks[block_idx].push(VarId(x + SIZE * y));
        }
    }

    let block_constraints = blocks.iter().flat_map(|block| all_diff(block));

    iter::empty()
        .chain(column_constraints)
        .chain(row_constraints)
        .chain(block_constraints)
        .collect()
}

fn main() {
    assert_eq!(BLOCK_SIZE * N_BLOCKS, SIZE);

    let prefilled_grid = vec![
        // Empty
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],

        // Easy
        vec![0, 4, 8, 0, 0, 0, 0, 7, 0],
        vec![2, 7, 0, 6, 9, 0, 0, 3, 0],
        vec![0, 3, 0, 0, 7, 2, 0, 4, 0],
        vec![3, 0, 0, 0, 0, 0, 4, 1, 0],
        vec![0, 9, 0, 1, 0, 8, 7, 0, 3],
        vec![5, 1, 6, 0, 4, 0, 0, 0, 8],
        vec![0, 2, 0, 0, 0, 9, 1, 0, 0],
        vec![7, 5, 4, 2, 0, 1, 3, 0, 6],
        vec![1, 0, 0, 7, 0, 5, 0, 0, 0],
        // Ruurd
        // vec![0, 3, 0, 0, 5, 0, 0, 0, 0],
        // vec![8, 0, 0, 7, 0, 0, 0, 9, 0],
        // vec![0, 0, 0, 0, 0, 8, 0, 6, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 0, 0, 0, 0, 0, 0, 0, 0],
        // vec![0, 9, 0, 8, 0, 0, 0, 0, 0],
        // vec![0, 2, 0, 0, 0, 7, 0, 0, 4],
        // vec![0, 0, 0, 0, 0, 6, 0, 5, 0],
    ];

    assert_eq!(prefilled_grid.len(), SIZE);
    assert!(prefilled_grid.iter().all(|r| r.len() == SIZE));

    let grid: Vec<Vec<Variable>> = prefilled_grid
        .into_iter()
        .enumerate()
        .map(|(y, row)| {
            row.into_iter()
                .enumerate()
                .map(|(x, value)| {
                    let id = VarId(x * SIZE + y);
                    let (lower, upper) = if value == 0 {
                        (1, SIZE as u32)
                    } else {
                        (value, value)
                    };
                    Variable::new(&id, lower, upper)
                })
                .collect()
        })
        .collect();

    let constraints = create_constraints(&grid);
    println!("num constraints {}", constraints.len());

    let variables: Vec<Variable> = grid.clone().into_iter().flat_map(|row| row).collect();

    let solution = solve(variables, &constraints);

    assert_eq!(solution.is_some(), true);

    if let Some(assignment) = solution {
        println!("Solved!");
        draw_solution(grid, assignment)
    } else {
        println!("No solution");
    }
}
