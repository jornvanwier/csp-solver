use crate::problem::VarValue;
use crate::solve::Assignment::{Assigned, Unassigned};
use crate::{BinaryConstraint, Variable};

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum Assignment {
    Assigned(VarValue),
    Unassigned,
}

impl Assignment {
    fn unwrap(&self) -> VarValue {
        match *self {
            Assigned(value) => value,
            Unassigned => panic!(".unwrap called on unassigned Assignment"),
        }
    }
}

pub fn solve(
    variables: Vec<Variable>,
    constraints: &Vec<BinaryConstraint>,
) -> Option<Vec<VarValue>> {
    let initial_assignment = variables.iter().map(|_| Assignment::Unassigned).collect();

    backtrack(initial_assignment, variables, constraints)
        .map(|assignment| assignment.iter().map(Assignment::unwrap).collect())
}

fn backtrack(
    mut assignment: Vec<Assignment>,
    mut unassigned_variables: Vec<Variable>,
    constraints: &[BinaryConstraint],
) -> Option<Vec<Assignment>> {
    if all_assigned(&assignment) {
        // Solution found
        // println!("yes");
        return Some(assignment);
    }

    let next_variable = select_variable(&mut unassigned_variables)?;
    let mut selected_variable = next_variable.clone();

    while selected_variable.has_next() {
        assignment[selected_variable.id] = Assigned(
            selected_variable
                .next_value()
                .expect("select_variable should not give empty domains"),
        );

        let relevant_constraints = constraints
            .iter()
            .filter(|c| c.uses_variable(&selected_variable));

        let consistent = test_solution(&assignment, relevant_constraints);

        if consistent {
            let backtrack_result = backtrack(
                assignment.clone(),
                unassigned_variables.clone(),
                constraints,
            );

            if backtrack_result.is_some() {
                return backtrack_result;
            }
        }
    }

    None
}

fn all_assigned(assignment: &[Assignment]) -> bool {
    assignment.iter().all(|a| *a != Unassigned)
}

fn test_solution<'a>(
    assignment: &Vec<Assignment>,
    constraints: impl Iterator<Item = &'a BinaryConstraint>,
) -> bool {
    constraints
        .filter(|c| {
            assignment[c.variables.0] != Unassigned && assignment[c.variables.1] != Unassigned
        })
        .all(|c| {
            c.is_satisfied_by(
                assignment[c.variables.0].unwrap(),
                assignment[c.variables.1].unwrap(),
            )
        })
}

fn select_variable(variables: &mut Vec<Variable>) -> Option<Variable> {
    variables.pop()
}
