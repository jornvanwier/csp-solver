use std::fmt;
use std::ops::{Index, IndexMut};

use itertools::Itertools;

use crate::solve::Assignment;

pub type VarValue = u32;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct VarId(pub usize);

impl Index<VarId> for Vec<Assignment> {
    type Output = Assignment;

    fn index(&self, index: VarId) -> &Assignment {
        &self[index.0]
    }
}

impl IndexMut<VarId> for Vec<Assignment> {
    fn index_mut(&mut self, index: VarId) -> &mut Self::Output {
        &mut self[index.0]
    }
}

impl AsRef<VarId> for VarId {
    fn as_ref(&self) -> &VarId {
        &self
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Domain {
    lower: VarValue,
    upper: VarValue,
}

impl Domain {
    fn next(&mut self) -> Option<VarValue> {
        if self.has_next() {
            let next_value = self.lower;
            self.lower += 1;
            Some(next_value)
        } else {
            None
        }
    }

    fn has_next(&self) -> bool {
        self.lower <= self.upper
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Variable {
    pub id: VarId,
    domain: Domain,
}

impl Variable {
    pub fn new<T: AsRef<VarId>>(id: T, lower: VarValue, upper: VarValue) -> Self {
        Self {
            id: *id.as_ref(),
            domain: Domain { lower, upper },
        }
    }

    pub fn next_value(&mut self) -> Option<VarValue> {
        self.domain.next()
    }

    pub fn has_next(&self) -> bool {
        self.domain.has_next()
    }
}

impl fmt::Display for Variable {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "var({:?})", self.id)
    }
}

impl AsRef<VarId> for Variable {
    fn as_ref(&self) -> &VarId {
        &self.id
    }
}

type BinarySatisfactionFn = fn(VarValue, VarValue) -> bool;

pub struct BinaryConstraint {
    pub variables: (VarId, VarId),
    function: BinarySatisfactionFn,
}

impl BinaryConstraint {
    pub fn new<T: AsRef<VarId>>(variables: (T, T), function: BinarySatisfactionFn) -> Self {
        Self {
            variables: (*variables.0.as_ref(), *variables.1.as_ref()),
            function,
        }
    }

    pub fn is_satisfied_by(&self, a: VarValue, b: VarValue) -> bool {
        (self.function)(a, b)
    }

    pub fn uses_variable<T: AsRef<VarId>>(&self, variable_id: T) -> bool {
        let variable_id = *variable_id.as_ref();
        self.variables.0 == variable_id || self.variables.1 == variable_id
    }
}

impl fmt::Display for BinaryConstraint {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let (a, b) = &self.variables;
        write!(f, "Constraint({:?}, {:?})", a, b)
    }
}

pub fn all_diff<T: AsRef<VarId> + PartialEq>(variables: &[T]) -> Vec<BinaryConstraint> {
    variables
        .iter()
        .cartesian_product(variables.iter())
        .filter(|(a, b)| a != b)
        .map(|(a, b)| BinaryConstraint::new((a, b), |a, b| a != b))
        .collect()
}
