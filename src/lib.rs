use problem::{BinaryConstraint, Variable};

pub mod problem;
pub mod solve;

#[cfg(test)]
mod tests {
    use crate::problem::{all_diff, VarId};
    use crate::solve::solve;
    use crate::{BinaryConstraint, Variable};

    #[test]
    fn four_different_numbers() {
        let count = 4u32;

        let variables: Vec<Variable> = (0..count)
            .into_iter()
            .map(|n| Variable::new(VarId(n as usize), 0, count))
            .collect();

        let mut constraints = all_diff(&variables);
        constraints.push(BinaryConstraint::new((&VarId(0), &VarId(1)), |a, b| a < b));
        constraints.push(BinaryConstraint::new((&VarId(1), &VarId(2)), |b, c| b > c));

        for x in constraints.iter() {
            println!("{}", x);
        }

        let solution = solve(variables, &constraints);
        println!("Solved {:?}", solution);

        assert_eq!(solution.is_some(), true);
    }

    #[test]
    fn domain() {
        let mut var = Variable::new(&VarId(0), 1, 5);

        let mut result = vec![];

        while var.has_next() {
            result.push(var.next_value().unwrap())
        }

        assert_eq!(result, vec![1, 2, 3, 4, 5]);
    }
}
